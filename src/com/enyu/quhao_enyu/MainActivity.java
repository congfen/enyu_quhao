package com.enyu.quhao_enyu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	/**
	 * 侧滑布局对象，用于通过手指滑动将左侧的菜单布局进行显示或隐藏。
	 */
	private SlidingLayout slidingLayout;

	/**
	 * menu按钮，点击按钮展示左侧布局，再点击一次隐藏左侧布局。
	 */
	private Button menuButton;

	/**
	 * 放在content布局中的ListView。
	 */
	private ListView contentListView;

	/**
	 * 作用于contentListView的适配器。
	 */
	private ArrayAdapter<String> contentListAdapter;

	/**
	 * 用于填充contentListAdapter的数据源。
	 */
	 private String[] mListTitle = { "外婆家", "外婆家", "外婆家"};  
	    private String[] mListStr = { "上海宜山路", "上海宜山路", "上海宜山路" };
	    
	    ArrayList<Map<String,Object>> mData= new ArrayList<Map<String,Object>>();;  
	    
	 //左侧菜单
	    private ImageView img_touxiang;
	    private TextView tv_nicheng,tv_qianming;
	    private ListView lv_setting;
	    
	    private String[] mListSetting={"精选","周边","设置"};
	    
	ArrayList<Map<String,Object>> mData_Setting= new ArrayList<Map<String,Object>>();;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);
		slidingLayout = (SlidingLayout) findViewById(R.id.slidingLayout);
		menuButton = (Button) findViewById(R.id.menuButton);
		contentListView = (ListView) findViewById(R.id.contentList);
		//左侧菜单
		img_touxiang=(ImageView)findViewById(R.id.img_touxiang);
		tv_nicheng=(TextView)findViewById(R.id.name);
		tv_qianming=(TextView)findViewById(R.id.qianming);
		lv_setting=(ListView)findViewById(R.id.lv_setting);
		
		
		img_touxiang.setBackgroundResource(R.drawable.waipojia);
		
		for(int i =0; i < mListSetting.length; i++) {  
	        Map<String,Object> item = new HashMap<String,Object>();  
	        item.put("image", R.drawable.setting);  
	        item.put("title", mListSetting[i]);  
	        mData_Setting.add(item);   
	    } 
		SimpleAdapter setting_adapter = new SimpleAdapter(this,mData_Setting,R.layout.menu_item,  
	            new String[]{"image","title"},new int[]{R.id.image_setting,R.id.title_setting});
		lv_setting.setAdapter(setting_adapter);
		
		
		//右侧布局
		 int lengh = mListTitle.length;  
		    for(int i =0; i < lengh; i++) {  
		        Map<String,Object> item = new HashMap<String,Object>();  
		        item.put("image", R.drawable.waipojia);  
		        item.put("title", mListTitle[i]);  
		        item.put("text", mListStr[i]);  
		        mData.add(item);   
		    }  
		    
		    SimpleAdapter adapter = new SimpleAdapter(this,mData,R.layout.main_item,  
		            new String[]{"image","title","text"},new int[]{R.id.image,R.id.title,R.id.text});
		contentListView.setAdapter(adapter);
		
		contentListView.setOnItemClickListener(new OnItemClickListener() {  
	        @Override  
	        public void onItemClick(AdapterView<?> adapterView, View view, int position,  
	            long id) {  
	        Toast.makeText(MainActivity.this,"您选择了标题：" + mListTitle[position] + "内容："+mListStr[position], Toast.LENGTH_LONG).show();  
	        }  
	    }); 
		// 将监听滑动事件绑定在contentListView上
		slidingLayout.setScrollEvent(contentListView);
		menuButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// 实现点击一下menu展示左侧布局，再点击一下隐藏左侧布局的功能
				if (slidingLayout.isLeftLayoutVisible()) {
					slidingLayout.scrollToRightLayout();
				} else {
					slidingLayout.scrollToLeftLayout();
				}
			}
		});
	}

}
